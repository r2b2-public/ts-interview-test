## Teoretická část

Otevřete si stránku https://formats.r2b2.cz/view/vhs04lwnemmy4q4 a pomocí devtools zodpovězte následující otázky. Není nutné stahovat a upravovat celý kód.

1. U elementu header.c-header upravte jednu hodnotu již existující CSS vlastnosti tak, aby byla hlavička vidět i při odscrollování. 

2. Hlavní nadpis - `h1` - má v inline stylu nastavenou barvu písma na černou. Proč se i přesto zobrazuje růžově?

3. Z jakého důvodu se nezobrazuje obrázek `.hero-media > .img > img` ?

4. Jak byste bez použití inline stylů obarvil/a na modro pozadí odstavce začínajícího větou “Typickou reakcí většiny divokých zvířat by byl útěk.”? 

5. Logo v hlavičce by mělo proklikávat na stránku https://tn.nova.cz/. Proč se tak reálně neděje?

## Praktická část

Dále si prosím vytvořte separátní statickou HTML stránku, ve které vytvořte reklamní baner o velikosti 480x300px. Tento baner nastylujte tak, aby vizuálně zapadal do stránky tn.cz. Součástí reklamy je vždy obrázek, nadpis a popisek
reklamy. 
Konkrétní obsah reklamy však není předem známý. Víme ale, že: 
- nadpis bude mít 30-90 znaků
- popisek bude mít až 120 znaků
- obrázek může mít nejrůznější rozměry a není možné jej jakkolilv deformovat nebo ořezávat.  

Z tohoto důvodu udělejte univerzální HTML šablonu, ve které se bude moci zobrazit libovolná reklama dle těchto pravidel. V šabloně použijte lorem ipsum a jakýkoliv univerzální obrázek. 

Rozložení prvků je čistě na vás. Do baneru můžete vložit i vlastní tlačítko s textem např. zobrazit, více informací apod.

Použité CSS vlastnosti musí být kompatibilní s běžně používanými moderními prohlížeči, ale zároveň i s prohlížečem Internet explorer 10. 
